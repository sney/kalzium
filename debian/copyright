Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kalzium
Upstream-Contact: Ian Monroe <ian.monroe@gmail.com>
                  Jeremy Paul Whiting <jpwhiting@kde.org>
Source: https://invent.kde.org/education/kalzium
Comment: This package was developed for Ubuntu by the Kubuntu Developers and
 modified to be included in Debian. See below the copyright holders for debian/*
 files.

Files: *
Copyright: 2016, Andreas Cord-Landwehr <cordlandwehr@kde.org>
           2007, Carste Niehaus <cniehaus@kde.org>
           2003-2008, Carsten Niehaus <cniehaus@kde.org>
           2010-2011, Etienne Rebetez <etienne.rebetez@oberwallis.ch>
           2006, Georges Khaznadar <georgesk@debian.org>
           2007, Ian Monroe <ian@monroe.nu>
           2005, Inge Wallin <inge@lysator.liu.se>
           2006-2007, Jerome Pansanel <j.pansanel@pansanel.net>
           2007, Johannes Simon <johannes.simon@gmail.com>
           2009, Kashyap R Puranik <kashthealien@gmail.com>
           2009, Kashyap. R. Puranik <kashthealien@gmail.com>
           2010, Luca Tringali <TRINGALINVENT@libero.it>
           2007-2009, Marcus D. Hanwell <marcus@cryos.org>
           2005-2007, Pino Toscano <pino@kde.org>
           2005-2006, Pino Toscano <toscano.pino@tiscali.it>
           2011, Rebetez Etienne <etienne.rebetez@oberwallis.ch>
           2004-2006, Thomas Nagy <tnagy2^8@yahoo.fr>
License: GPL-2.0-or-later

Files: cmake/modules/FindLibfacile.cmake
       cmake/modules/FindOCaml.cmake
       cmake/modules/FindOpenBabel2.cmake
       src/psetable/statemachine.h
Copyright: 2006-2007, Carsten Niehaus <cniehaus@gmx.de>
           2008, Marcus D. Hanwell <marcus@cryos.org>
           2006, Montel Laurent <montel@kde.org>
           2010, Nokia Corporation and /or its subsidiary(-ies) <qt-info@nokia.com>
           2006-2009, Pino Toscano <pino@kde.org>
License: BSD-3-Clause
 
Files: org.kde.kalzium.appdata.xml
Copyright: 2014, Andreas Cord-Landwehr <cordlandwehr@kde.org>
License: CC0-1.0

Files: doc/*
Copyright: 2001-2008, Carsten Niehaus <cniehaus@kde.org>
           2005, Anne-Marie Mahfouf
License: GFDL-1.2-only

Files: src/psetable/elementitem.cpp
       src/psetable/elementitem.h
       src/psetable/numerationitem.cpp
       src/psetable/numerationitem.h
       src/psetable/periodictablescene.cpp
       src/psetable/periodictablescene.h
       src/psetable/statemachine.cpp
Copyright: 2003-2006, Carsten Niehaus <cniehaus@kde.org>
           2010, Etienne Rebetez <etienne.rebetez@oberwallis.ch>
           2007-2009, Marcus D. Hanwell <marcus@cryos.org>
           2005-2006, Pino Toscano <toscano.pino@tiscali.it>
License: LGPL-2.1-or-later

Files: debian/*
Copyright: 2011-2022, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
           2011, Kubuntu Developers <kubuntu-devel@lists.launchpad.net>
License: GPL-2.0-or-later

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all
 copyright and related and neighboring rights to this software to the
 public domain worldwide. This software is distributed without any
 warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication
 along with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 Public Domain
 Dedication can be found in `/usr/share/common-licenses/CC0-1.0’.

License: GFDL-1.2-only
 Permission is granted to copy, distribute and/or modify this
 document under the terms of the GNU Free Documentation License
 Version 1.2; with no Invariant Sections, no Front-Cover Texts, and
 no Back-Cover Texts.
 .
 On Debian systems, the complete text of the GNU Free Documentation
 License version 1.2 can be found in
 `/usr/share/common-licenses/GFDL-1.2’.

License: GPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA. Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2’.

License: LGPL-2.1-or-later
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301  USA Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 `/usr/share/common-licenses/LGPL-2.1’.
